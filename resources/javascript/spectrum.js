$(document).ready(function( ) {
	$(document).on("click", ".main.main-small .card", function( ) {
		$(this).toggleClass("closed");
	});
	
	$.get("../resources/templates/card.tpl", function(card) {
		$.get("../resources/templates/card-more.tpl", function(card_more) {
			$.getJSON("configuration.json", function(configuration) {
				$(".content").empty( );
				
				$.each(configuration, function(index, value) {
					let section = index;
					
					$.each(value, function(index, value) {
						let template = card;
						
						template = template.replace("{{ title }}", value.title);
						template = template.replace("{{ description }}", value.description);
						template = template.replace("{{ status }}", value.status);
						
						template = template.replace("{{ more-length }}", value.more ? "<span class=\"badge\">" + value.more.length + "</span>" : " ");
						
						if(value.more) {
							let more = " ";
							
							$.each(value.more, function(index, value) {
								let template = card_more;
						
								template = template.replace("{{ title }}", value.title);
								template = template.replace("{{ description }}", value.description);
								template = template.replace("{{ status }}", value.status);
								
								more += template;
							});
							
							template = template.replace("{{ more }}", "<div class=\"more\">" + more + "</div>");
						} else {
							template = template.replace("{{ more }}", " ");
						}
						
						$(".content[data-section='" + section + "']").append(template);
					});
				});
			});
		});
	});
});