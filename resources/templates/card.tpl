<div class="card closed">
	<div class="card-body">
		<span class="{{ status }}">
			<h4 class="card-title">{{ title }} {{ more-length }}</h4>
			<p class="card-text">{{ description }}</p>
		</span>
		
		{{ more }}
	</div>
</div>